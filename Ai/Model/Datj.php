<?php
 
namespace OpenTag\Ai\Model;
 
class Datj extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
 
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        parent::__construct($context);
        $this->_productCollectionFactory = $productCollectionFactory;
    }
	
    public function getProductCollectionByCategories()
    {
        //$categories = [2,3,5];//category ids array
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        //$collection->addCategoriesFilter(['in' => $categories]);
        return $collection;
    }
}